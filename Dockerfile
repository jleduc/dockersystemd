FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
MAINTAINER julien.leduc@cern.ch

RUN rm -f /etc/rc.d/rc.local

ADD rc.local /etc/rc.d

ADD etc_selinux_config /etc/selinux/config

RUN chmod 744 /etc/rc.local

# Needed for ansible
RUN yum install -y sudo

# so that we can use systemd
VOLUME [ "/sys/fs/cgroup" ]

ENTRYPOINT ["/usr/sbin/init"]
